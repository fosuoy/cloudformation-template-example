## Clojure-Collector deployment on loaclhost

This playbook installs / configures tomcat8 then deploys a simple WAR application

This is intended to be used in conjunction with a CloudFormation template which
configures the infrastructure.

When the playbook run completes, you should be able to see the Tomcat
Application Server running the Clojure-Collector application.
