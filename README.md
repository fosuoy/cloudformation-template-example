## This is a CloudFormation / Ansible combination to bring up a tomcat8 server with a deployed application on it.

When running the cloudformation template, please be sure to set the following

variables:

KeyName

DebianRelease (Tested on Debian Jessie ( DebianJessieHvmEbs ))

OperatorEmail (To recieve high load alerts)

InstanceType (e.g. t2.micro)



The template includes instruction on downloading the playbook and running it.

To run the template, the most straightforward way is using Amazons AWS cli

interface, available from pip (`pip install aws-cli`).



For instruction on configuring aws-cli with your credentials see here:

http://docs.aws.amazon.com/cli/latest/reference/cloudformation/


Otherwise run:


`aws cloudformation create-stack --stack-name ClojureTestStack  --template-body file:///<location_of_template.json>   --parameters ParameterKey=KeyName,ParameterValue=<INSERT_KEY_NAME>               ParameterKey=DebianRelease,ParameterValue=DebianJessieHvmEbs                ParameterKey=OperatorEmail,ParameterValue=example@example.com                ParameterKey=InstanceType,ParameterValue=t2.micro`
